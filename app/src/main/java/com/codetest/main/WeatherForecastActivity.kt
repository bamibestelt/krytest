package com.codetest.main

import android.app.AlertDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.*
import com.codetest.R
import com.codetest.main.model.Location
import com.codetest.main.model.Status
import com.codetest.main.ui.LocationViewHolder
import kotlinx.android.synthetic.main.activity_main.*


class WeatherForecastActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "WeatherForecastActivity"
    }

    private var adapter = ListAdapter()
    private var locations: ArrayList<Location> = arrayListOf()
    private var selectedStatus = Status.values().first()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        adapter = ListAdapter()
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        item?.let {
            if (item.itemId == R.id.menu_add_location) {
                showAddLocationDialog()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onResume() {
        super.onResume()
        fetchLocations()
    }


    private fun addLocation(city: String, temp: String) {
        val location = Location("", city, temp, selectedStatus)
        LocationHelper.addLocation(location) { response ->
            if (response == null) {
                showError()
            } else {
                val last = locations.size
                locations.add(response)
                adapter.notifyItemInserted(last)
            }
        }
    }


    private fun removeLocation(location: Location, position: Int) {
        location.id?.let {
            LocationHelper.removeLocation(it) { response ->
                if (response == null) {
                    showError()
                } else {
                    locations.remove(location)
                    adapter.notifyItemRemoved(position)
                }
            }
        }
    }


    private fun fetchLocations() {
        LocationHelper.getLocations { response ->
            if (response == null) {
                showError()
            } else {
                locations = response as ArrayList<Location>
                adapter.notifyDataSetChanged()
            }
        }
    }


    private fun showError() {
        AlertDialog.Builder(this)
                .setTitle(resources.getString(R.string.error_title))
                .setMessage(resources.getString(R.string.error_title))
                .setPositiveButton(resources.getString(R.string.ok), { _, _ -> })
                .create()
                .show()
    }


    private fun showAddLocationDialog() {
        val dialog = AlertDialog.Builder(this).create()

        val view = LayoutInflater.from(this).inflate(R.layout.dialog_add_location, null)

        val edCityName = view.findViewById<EditText>(R.id.ed_city_name)
        val edCityTemp = view.findViewById<EditText>(R.id.ed_city_temperature)

        val statusSpinner = view.findViewById<Spinner>(R.id.status_spinner)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, Status.values())
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        statusSpinner.adapter = adapter
        statusSpinner.onItemSelectedListener = selectionListener

        val btnAdd = view.findViewById<Button>(R.id.btn_add)
        btnAdd.setOnClickListener {
            val city = edCityName.text.toString()
            val temp = edCityTemp.text.toString()
            if (city.isNotBlank() && temp.isNotBlank()) {
                addLocation(city, temp)
                dialog.dismiss()
            }
        }

        dialog.setView(view)
        dialog.show()
    }


    private val selectionListener = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(p0: AdapterView<*>?) {}

        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
            selectedStatus = Status.values()[p2]
        }
    }


    private inner class ListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        override fun getItemCount(): Int {
            return locations.size
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return LocationViewHolder.create(parent)
        }

        override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
            (viewHolder as? LocationViewHolder)?.setup(locations[position])
            (viewHolder as? LocationViewHolder)?.removeLocationView?.setOnClickListener {
                removeLocation(locations[position], position)
            }
        }
    }


}