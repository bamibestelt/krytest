package com.codetest.main

import android.util.Log
import com.codetest.main.api.LocationApiService
import com.codetest.main.model.Location
import java.util.*

class LocationHelper {

    companion object {

        private const val TAG = "LocationHelper"
        private const val MAX_RETRY = 5

        fun getLocations(retry: Int = 0, callback: (List<Location>?) -> Unit) {
            if(retry == MAX_RETRY) {
                callback(null)
                return
            }

            val locations: ArrayList<Location> = arrayListOf()
            val apiKey = KeyUtil().getKey()
            LocationApiService.getApi().get(apiKey, "locations", {
                val list = it.get("locations").asJsonArray
                for (json in list) {
                    locations.add(Location.from(json.asJsonObject))
                }
                Log.d(TAG, "getLocations: success")
                callback(locations)
            }, {
                // fail? retry
                Log.e(TAG, "getLocations: retry")
                getLocations(retry + 1, callback)
            })
        }


        fun addLocation(location: Location, retry: Int = 0, callback: (Location?) -> Unit) {
            if(retry == MAX_RETRY) {
                callback(null)
                return
            }

            val apiKey = KeyUtil().getKey()

            LocationApiService.getApi().post(apiKey, "locations", location, {
                Log.d(TAG, "addLocation: success")
                callback(it)
            }, {
                Log.e(TAG, "addLocation: retry")
                addLocation(location, retry + 1, callback)
            })
        }


        fun removeLocation(id: String, retry: Int = 0, callback: (String?) -> Unit) {
            if(retry == MAX_RETRY) {
                callback(null)
                return
            }

            val apiKey = KeyUtil().getKey()

            LocationApiService.getApi().delete(apiKey, "locations/$id", {
                Log.d(TAG, "removeLocation: success")
                callback(id)
            }, {
                Log.d(TAG, "removeLocation: retry")
                removeLocation(id, retry + 1, callback)
            })
        }


    }
}